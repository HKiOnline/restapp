# REST App

REST App is a template project for Java REST API app. It uses Google's Guice for dependency injection as well as TestNG and Mockito for testing. 
The project uses gradle for build automation and dependency management. Netflix Archaius for property file management. Archaius enables different setups in different environments.

## Running locally
You can run the app locally by executing the start-script. It launches jettyRun gradle-task.

    ./start

## Tests
Run unit tests with:

    ./gradlew test


Run integration tests with:

    ./gradlew integrationTests

Note that you need to have a ready instance when you run the integration tests.

## MVP (version 1) goals

 - Services loosely coupled to REST end-points ✔︎
 - Dependency injection ✔︎
 - Runnable locally (easily) ✔︎
 - Framework for testing ✔︎
 - Dockerized (TODO)