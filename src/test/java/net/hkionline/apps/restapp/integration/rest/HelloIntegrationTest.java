package net.hkionline.apps.restapp.integration.rest;

import org.testng.Assert;
import org.testng.annotations.Test;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class HelloIntegrationTest {

    private static String HELLO_BASE_URL = "http://localhost:8080/restapp/hello";

    @Test
    public void testHello() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(HELLO_BASE_URL);
        String response = webTarget.request().get(String.class);
        Assert.assertEquals(response, "moro");
    }

    @Test
    public void testHelloJohn() throws Exception {
        Client client = ClientBuilder.newClient();

        String fullUrl = HELLO_BASE_URL + "/john";

        WebTarget webTarget = client.target(fullUrl);
        String response = webTarget.request().get(String.class);
        Assert.assertEquals(response, "moro john");
    }
}
