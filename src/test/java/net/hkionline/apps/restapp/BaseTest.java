package net.hkionline.apps.restapp;


import com.netflix.config.ConfigurationManager;
import org.testng.annotations.BeforeTest;

public class BaseTest {

    @BeforeTest
    public void setUp() throws Exception{
        ConfigurationManager.getConfigInstance().setProperty("@environment", "test");
        ConfigurationManager.loadCascadedPropertiesFromResources("app");
    }

}
