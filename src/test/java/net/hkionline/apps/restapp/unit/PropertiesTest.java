package net.hkionline.apps.restapp.unit;

import com.netflix.config.DynamicPropertyFactory;
import com.netflix.config.DynamicStringProperty;
import net.hkionline.apps.restapp.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;



public class PropertiesTest extends BaseTest {

    public static final String SAMPLE_PROPERTY_STR =  "sampleprop";
    public static final String SAMPLE_PROPERTY_VALUE =  "propvalue";
    public static final String TEST_PROPERTY_STR =  "testProperty";
    public static final String TEST_PROPERTY_VALUE =  "testWorks";

    @Test
    public void readPropertyTest(){

        DynamicPropertyFactory properties = DynamicPropertyFactory.getInstance();

        DynamicStringProperty sampleProperty = properties.getStringProperty(SAMPLE_PROPERTY_STR, null);
        DynamicStringProperty testProperty = properties.getStringProperty(TEST_PROPERTY_STR, null);


        Assert.assertEquals(sampleProperty.get(), SAMPLE_PROPERTY_VALUE);
        Assert.assertEquals(testProperty.get(), TEST_PROPERTY_VALUE);
    }

}
