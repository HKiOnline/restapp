package net.hkionline.apps.restapp;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;


@ApplicationPath("/")
public class App extends ResourceConfig {
    public static final String SERVLET_PACKAGE_REFERENCE = "net.hkionline.apps.restapp.rest";

    public App(){
        packages(SERVLET_PACKAGE_REFERENCE);
    }
}
