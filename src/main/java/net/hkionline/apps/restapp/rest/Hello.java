package net.hkionline.apps.restapp.rest;

import com.google.inject.Guice;
import com.google.inject.Injector;
import net.hkionline.apps.restapp.core.service.ideal.GreetingService;
import net.hkionline.apps.restapp.core.service.ideal.NamedGreetingService;
import net.hkionline.apps.restapp.core.service.module.NamedTampereGreetingModule;
import net.hkionline.apps.restapp.core.service.module.TampereGreetingModule;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/hello")
public class Hello {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response hello(){

        Injector injector = Guice.createInjector(new TampereGreetingModule());
        GreetingService greetingService = injector.getInstance(GreetingService.class);

        return Response.ok(greetingService.greet()).build();
    }

    @Path("/{name}")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response helloJohn(@PathParam("name") final String name) {

        Injector injector = Guice.createInjector(new NamedTampereGreetingModule());
        NamedGreetingService namedGreetingService = injector.getInstance(NamedGreetingService.class);

        return Response.ok( namedGreetingService.greet(name) ).build();
    }

}
