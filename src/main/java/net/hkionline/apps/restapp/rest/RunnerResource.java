package net.hkionline.apps.restapp.rest;

import com.google.inject.Guice;
import com.google.inject.Injector;
import net.hkionline.apps.restapp.core.service.ideal.RunnerService;
import net.hkionline.apps.restapp.core.service.module.JavaScriptRunnerModule;


import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;


@Path("/runner")
public class RunnerResource {


    @POST
    @Consumes("application/javascript")
    @Produces("text/plain")
    public Response runScriptResource(String javascriptAsString){

        Injector injector = Guice.createInjector(new JavaScriptRunnerModule());
        RunnerService runnerService = injector.getInstance(RunnerService.class);

        String response = runnerService.runScript(javascriptAsString);

        return Response.ok(response).build();
    }


}
