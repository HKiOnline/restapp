package net.hkionline.apps.restapp.core.service.implementation;

import net.hkionline.apps.restapp.core.service.ideal.GreetingService;

/**
 * Created by hkesseli on 5/6/16.
 */
public class Moro implements GreetingService {

    @Override
    public String greet() {
        return "moro";
    }
}
