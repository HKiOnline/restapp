package net.hkionline.apps.restapp.core.service.ideal;

/**
 * Created by hkesseli on 5/6/16.
 */
public interface NamedGreetingService {
    String greet(String name);
}
