package net.hkionline.apps.restapp.core.service.module;

import com.google.inject.AbstractModule;
import net.hkionline.apps.restapp.core.service.ideal.GreetingService;
import net.hkionline.apps.restapp.core.service.implementation.Moro;

/**
 * Created by hkesseli on 5/6/16.
 */
public class TampereGreetingModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(GreetingService.class).to(Moro.class);
    }
}
