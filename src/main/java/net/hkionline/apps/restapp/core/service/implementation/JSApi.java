package net.hkionline.apps.restapp.core.service.implementation;

import jdk.nashorn.api.scripting.AbstractJSObject;

import java.util.HashMap;

public class JSApi extends AbstractJSObject {

    private HashMap<String, Object> dynamicMembers;

    public JSApi(){
        dynamicMembers = new HashMap<>();
    }

    @Override
    public Object getMember(String name) {
        return dynamicMembers.get(name);
    }

    @Override
    public void setMember(String name, Object value) {
        dynamicMembers.put(name, value);
    }

    @Override
    public void removeMember(String name) {
        dynamicMembers.remove(name);
    }

}
