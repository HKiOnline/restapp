package net.hkionline.apps.restapp.core.service.implementation;

import net.hkionline.apps.restapp.core.service.ideal.GreetingService;

public class Hello implements GreetingService {

    @Override
    public String greet() {
        return "hello";
    }
}
