package net.hkionline.apps.restapp.core.service.ideal;


public interface RunnerService {
    String runScript(String scriptAsJavaString);
}
