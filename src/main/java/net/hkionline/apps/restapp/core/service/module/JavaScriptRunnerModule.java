package net.hkionline.apps.restapp.core.service.module;

import com.google.inject.AbstractModule;
import net.hkionline.apps.restapp.core.service.ideal.RunnerService;
import net.hkionline.apps.restapp.core.service.implementation.JavaScriptRunner;

public class JavaScriptRunnerModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(RunnerService.class).to(JavaScriptRunner.class);
    }

}
