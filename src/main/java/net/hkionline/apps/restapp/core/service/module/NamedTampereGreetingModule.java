package net.hkionline.apps.restapp.core.service.module;

import com.google.inject.AbstractModule;
import net.hkionline.apps.restapp.core.service.ideal.GreetingService;
import net.hkionline.apps.restapp.core.service.ideal.NamedGreetingService;
import net.hkionline.apps.restapp.core.service.implementation.Moro;
import net.hkionline.apps.restapp.core.service.implementation.NamedGreeting;

/**
 * Created by hkesseli on 5/6/16.
 */
public class NamedTampereGreetingModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(GreetingService.class).to(Moro.class);
        bind(NamedGreetingService.class).to(NamedGreeting.class);
    }
}
