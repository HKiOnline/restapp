package net.hkionline.apps.restapp.core.service.implementation;

import com.google.inject.Inject;
import net.hkionline.apps.restapp.core.service.ideal.GreetingService;
import net.hkionline.apps.restapp.core.service.ideal.NamedGreetingService;

/**
 * Created by hkesseli on 5/6/16.
 */
public class NamedGreeting implements NamedGreetingService {

    private final GreetingService greetingService;

    @Inject
    public NamedGreeting(GreetingService greetingService){
        this.greetingService = greetingService;
    }

    @Override
    public String greet(String name) {
        return greetingService.greet() + " " + name;
    }
}
