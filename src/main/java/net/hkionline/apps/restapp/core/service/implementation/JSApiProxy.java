package net.hkionline.apps.restapp.core.service.implementation;

import jdk.nashorn.api.scripting.AbstractJSObject;

public class JSApiProxy extends AbstractJSObject {

    @Override
    public JSApi newObject(Object... args) {
        return new JSApi();
    }
}
