package net.hkionline.apps.restapp.core.service.implementation;


import jdk.nashorn.api.scripting.NashornScriptEngineFactory;
import net.hkionline.apps.restapp.core.service.ideal.RunnerService;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import java.io.*;


public class JavaScriptRunner implements RunnerService{

    private static final String PROXY_BINDING_NAME = "Service";

    public String runScript(String scriptAsJavaString){

        NashornScriptEngineFactory factory = new NashornScriptEngineFactory();
        ScriptEngine engine = factory.getScriptEngine(getEngineArguments());

        engine = setBindings(engine);

        StringWriter stringWriter = new StringWriter();
        engine.getContext().setWriter(stringWriter);

        try {
            engine.eval( getLoDash() );
            engine.eval( scriptAsJavaString );
            return stringWriter.toString();

        } catch ( Exception e ){
            System.out.println("Running script failed. Reason: "+e.getMessage());
        }

        return null;
    }

    private String[] getEngineArguments(){
        return new String[] {
                "-strict",
                "--no-java",
                "--no-syntax-extensions"
        };
    }

    private ScriptEngine setBindings(ScriptEngine engine){

        Bindings bindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);

        bindings.put(PROXY_BINDING_NAME, new JSApiProxy());

        bindings.put("API_VERSION", "1.0");
        bindings.put("LODASH_VERSION", "4.13.1");


        return engine;
    }

    private InputStreamReader getLoDash() throws FileNotFoundException {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("lodash.min.js").getFile());

        return new InputStreamReader( new FileInputStream( file ));
    }

}
