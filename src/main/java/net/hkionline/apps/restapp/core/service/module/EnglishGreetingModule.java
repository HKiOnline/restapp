package net.hkionline.apps.restapp.core.service.module;

import com.google.inject.AbstractModule;
import net.hkionline.apps.restapp.core.service.ideal.GreetingService;
import net.hkionline.apps.restapp.core.service.implementation.Hello;

/**
 * Created by hkesseli on 5/6/16.
 */

public class EnglishGreetingModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(GreetingService.class).to(Hello.class);
    }
}
